module.exports = () => ({
  corsAllowedOrigins: [
    'http://localhost:3000',
    'http://192.168.178.32:3000',
    'https://vicpah.gitlab.io',
    'https://vicpah.com.au',
    'https://www.vicpah.com.au',
    'https://vicpah.org.au',
    'https://www.vicpah.org.au',
  ],
});
