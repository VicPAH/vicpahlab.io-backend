'use strict';
const S3 = require('aws-sdk/clients/s3');
const axios = require('axios');
const cheerio = require('cheerio');
const { createHash } = require('crypto');
const Eckles = require('eckles');
const FormData = require('form-data');
const escape = require('escape-html');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const process = require('process');
const Sentry = require('@sentry/node');
const sharp = require('sharp');
const util = require('util');
const { v4: uuid } = require('uuid');

const config = require('./config')();


const MIN_UPLOAD_SIZE = 1;
const MAX_UPLOAD_SIZE = 1000 * 1000 * 20; // 20Mb

const HEX_RE = /^[a-fA-F0-9]+$/;
const BEARER_RE = /Bearer\s+([^\s]+)/;

const TOKEN_OPTS = {
  audience: 'au.org.vicpah.photocomp',
  issuer: 'au.org.vicpah.photocomp',
};
const TOKEN_CREATE_OPTS = {
  ...TOKEN_OPTS,
  algorithm: 'RS256',
};
const TOKEN_VERIFY_OPTS = {
  ...TOKEN_OPTS,
  algorithms: ['RS256'],
};

const EMPTY_BUFFER = Buffer.from('');


function bigLog(...args) {
  console.log(...args.map(arg => _.isString(arg) ? arg : util.inspect(arg, false, null)))
}


async function s3GetHelper(s3, bucket, key) {
  const payload = {
    Bucket: bucket,
    Key: key,
  };
  bigLog('s3.getObject', payload);
  try {
    return await s3.getObject(payload).promise();
  } catch(err) {
    if (err.code === 'NoSuchKey') {
      return null;
    }
    throw err;
  }
}
async function s3ListHelper(s3, bucket, prefix) {
  const payload = {
    Bucket: bucket,
    Prefix: prefix,
  };
  bigLog('s3.listObjects', payload);
  const data = await s3.listObjects(payload).promise();
  if (data.IsTruncated) {
    throw new Error('Pagination not implemented');
  }
  return data;
}
async function s3ListKeysHelper(s3, bucket, prefix) {
  return (await s3ListHelper(s3, bucket, prefix)).Contents.map(obj => obj.Key);
}
async function s3ListCountHelper(s3, bucket, prefix) {
  return (await s3ListHelper(s3, bucket, prefix)).Contents.length;
}
async function s3DeletesHelper(s3, bucket, keys) {
  if (keys.length === 0) {
    return {};
  }
  const payload = {
    Bucket: bucket,
    Delete: {
      Objects: keys.map(key => ({ Key: key })),
      Quiet: true,
    },
  }
  bigLog('s3.deleteObjects', payload);
  return s3.deleteObjects(payload).promise();
}
async function jsonGetHelper(s3, bucket, key) {
  const data = await s3GetHelper(s3, bucket, key);
  if (!data) {
    return data;
  }
  return JSON.parse(data.Body);
}
async function modelGetHelper(s3, bucket, key, klass, extra={}) {
  const data = await jsonGetHelper(s3, bucket, key);
  if (!data) {
    return data;
  }
  return new klass({ ...data, ...extra, s3 });
}
async function jsonPutHelper(s3, bucket, key, data) {
  const payload = {
    Bucket: bucket,
    Key: key,
    ContentType: 'application/json',
    Body: JSON.stringify(data),
  }
  bigLog('s3.putObject', payload);
  return await s3.putObject({ ...payload, Body: Buffer.from(payload.Body) }).promise();
}
async function emptyPutHelper(s3, bucket, key) {
  const payload = {
    Bucket: bucket,
    Key: key,
    Body: EMPTY_BUFFER,
  };
  bigLog('s3.putObject', payload);
  return await s3.putObject(payload).promise();
}

class Photo {
  constructor(data) {
    this._origData = data;
    Object.assign(this, data);
    this.toJSON = this.toJSON.bind(this);
    this.save = this.save.bind(this);
    this.gets3 = this.gets3.bind(this);
  }

  gets3() {
    if (!this.s3) {
      this.s3 = new S3();
    }
    return this.s3;
  }

  toJSON() {
    return _.pick(this, [
      'user_sub',
      'user_ip',
      'name',
      'email',
      'caption',
      'hash',
      'ready',
    ]);
  }

  save() {
    const proms = [
      jsonPutHelper(
        this.gets3(),
        process.env.PHOTO_COMP_METADATA_BUCKET,
        `photos/${this.hash}.json`,
        _.omit(this.toJSON(), ['hash']),
      ),
      emptyPutHelper(
        this.gets3(),
        process.env.PHOTO_COMP_METADATA_BUCKET,
        `photos_by_user_sub/${this.user_sub}/${this.hash}`,
      ),
      emptyPutHelper(
        this.gets3(),
        process.env.PHOTO_COMP_METADATA_BUCKET,
        `photos_by_user_ip/${this.user_ip}/${this.hash}`,
      ),
    ];
    if (this._origData) {
      const toDelete = [];
      for (const key of ['user_sub', 'user_ip']) {
        if (this._origData[key] && this._origData[key] !== this[key]) {
          toDelete.push(`photos_by_${key}/${this._origData[key]}/${this.hash}`);
        }
      }
      proms.push(s3DeletesHelper(
        this.gets3(),
        process.env.PHOTO_COMP_METADATA_BUCKET,
        toDelete,
      ));
    }
    return Promise.all(proms);
  }

  static byHash(hash) {
    return modelGetHelper(
      new S3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `photos/${hash}.json`,
      Photo,
      { hash },
    )
  }

  static countByUserSub(value) {
    return s3ListCountHelper(
      new S3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `photos_by_user_sub/${value}/`,
    );
  }
  static countByUserIp(value) {
    return s3ListCountHelper(
      new S3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `photos_by_user_ip/${value}/`,
    );
  }
  static listByHashPrefix(value) {
    return s3ListKeysHelper(
      new S3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `photos/${value}`,
    );
  }
  static listAll() {
    return Photo.listByHashPrefix('');
  }
  static async listAllHashes() {
    return (await Photo.listAll()).map(key => (
      key.substring(key.lastIndexOf('/') + 1, key.lastIndexOf('.'))
    ))
  }
}

class VoteOne {
  constructor(data) {
    this._origData = data;
    Object.assign(this, data);
    this.toJSON = this.toJSON.bind(this);
    this.save = this.save.bind(this);
    this.gets3 = this.gets3.bind(this);
  }

  gets3() {
    if (!this.s3) {
      this.s3 = new S3();
    }
    return this.s3;
  }

  toJSON() {
    return _.pick(this, [
      'user_sub',
      'user_ip',
      'hash',
    ]);
  }

  save() {
    return jsonPutHelper(
      this.gets3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `voteones/${this.hash}/${this.user_sub}.json`,
      _.omit(this.toJSON(), ['hash', 'user_sub']),
    );
  }
  static countByHash(value) {
    return s3ListCountHelper(
      new S3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `voteones/${value}/`,
    );
  }
}
class VoteRace {
  constructor(data) {
    this._origData = data;
    Object.assign(this, data);
    this.toJSON = this.toJSON.bind(this);
    this.save = this.save.bind(this);
    this.gets3 = this.gets3.bind(this);
  }

  gets3() {
    if (!this.s3) {
      this.s3 = new S3();
    }
    return this.s3;
  }

  toJSON() {
    return _.pick(this, [
      'winner_hash',
      'other_hash',
      'user_sub',
      'user_ip',
    ]);
  }

  save() {
    const ordered = [this.winner_hash, this.other_hash].sort();
    return Promise.all([
      jsonPutHelper(
        this.gets3(),
        process.env.PHOTO_COMP_METADATA_BUCKET,
        `voterace_by_user_sub/${this.user_sub}/${ordered[0]}/${ordered[1]}.json`,
        _.omit(this.toJSON(), ['user_sub']),
      ),
      emptyPutHelper(
        this.gets3(),
        process.env.PHOTO_COMP_METADATA_BUCKET,
        `voterace_by_winner_hash/${this.winner_hash}/${this.other_hash}/${this.user_sub}`,
      ),
      emptyPutHelper(
        this.gets3(),
        process.env.PHOTO_COMP_METADATA_BUCKET,
        `voterace_by_other_hash/${this.other_hash}/${this.winner_hash}/${this.user_sub}`,
      ),
      s3DeletesHelper(
        this.gets3(),
        process.env.PHOTO_COMP_METADATA_BUCKET,
        [`voteracelock/${this.user_sub}.json`],
      ),
    ]);
  }
  static byUserSubAndHashes(userSub, leftHash, rightHash) {
    const ordered = [leftHash, rightHash].sort();
    return modelGetHelper(
      new S3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `voterace_by_user_sub/${userSub}/${ordered[0]}/${ordered[1]}.json`,
      VoteRace,
      { user_sub: userSub },
    )
  }
  static listByUserSub(value) {
    return s3ListKeysHelper(
      new S3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `voterace_by_user_sub/${value}/`,
    );
  }
  static async listHashesByUserSub(value) {
    return (await VoteRace.listByUserSub(value)).map(key => {
      const [prefix, userSub, leftHash, rightHashFile] = key.split('/');
      return [leftHash, rightHashFile.substring(0, rightHashFile.lastIndexOf('.'))];
    });
  }
}
class VoteRaceLock {
  constructor(data) {
    this._origData = data;
    Object.assign(this, data);
    this.toJSON = this.toJSON.bind(this);
    this.save = this.save.bind(this);
    this.gets3 = this.gets3.bind(this);
  }

  gets3() {
    if (!this.s3) {
      this.s3 = new S3();
    }
    return this.s3;
  }

  toJSON() {
    return _.pick(this, [
      'user_sub',
      'hashes',
    ]);
  }

  save() {
    return jsonPutHelper(
      this.gets3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `voteracelock/${this.user_sub}.json`,
      _.omit(this.toJSON(), ['user_sub']),
    );
  }

  static byUserSub(value) {
    return modelGetHelper(
      new S3(),
      process.env.PHOTO_COMP_METADATA_BUCKET,
      `voteracelock/${value}.json`,
      VoteRace,
      { user_sub: value },
    )
  }
}


function getHeader(headers, wanted) {
  const wantedLc = wanted.toLowerCase()
  for (const [key, value] of Object.entries(headers)) {
    if (key.toLowerCase() === wantedLc) {
      return value
    }
  }
}

async function checkToken(event, subAud=null) {
  const state = {};
  const auth = getHeader(event.headers, 'authorization');
  if (!auth) {
    console.error('No auth header', event.headers);
    console.log(event);
    return { ...state, provided: false };
  }
  state.provided = true
  if (!auth.startsWith('Bearer ')) {
    console.error('Not a bearer auth', auth);
    return { ...state, signed: false };
  }

  let token;
  try {
    token = auth.match(BEARER_RE)[1];
  } catch(err) {
    console.error('Auth token regex mismatch', auth);
    return { ...state, signed: false };
  }
  if (!token) {
    console.error('No token');
    return { ...state, signed: false };
  }

  const jwkJSON = JSON.parse(Buffer.from(process.env.MEMBER_PROFILES_JWK_B64, 'base64'));
  const jwkPEM = await Eckles.toPem({ jwk: jwkJSON.public });

  const thisVerify = {
    ...TOKEN_VERIFY_OPTS,
    ...(subAud ? { audience: `${TOKEN_VERIFY_OPTS.audience}.${subAud}` } : {}),
  }

  let payload;
  try {
    state.payload = jwt.verify(token, jwkPEM, thisVerify);
    state.signed = true;
  } catch(err) {
    console.error('Verification failed', err);
    return { ...state, signed: false };
  }
  try {
    jwt.verify(token, jwkPEM, { ...thisVerify, maxAge: '24 hours' });
  } catch(err) {
    return { ...state, old: true };
  }
  return { ...state, old: false };
}
async function createToken(subject, subAud=null) {
  const jwkJSON = JSON.parse(Buffer.from(process.env.MEMBER_PROFILES_JWK_B64, 'base64'));
  const jwkPEM = await Eckles.toPem({ jwk: jwkJSON.private });

  const thisCreate = {
    ...TOKEN_CREATE_OPTS,
    subject,
    ...(subAud ? { audience: `${TOKEN_CREATE_OPTS.audience}.${subAud}` } : {}),
  }

  return jwt.sign({}, jwkPEM, thisCreate);
}

function getOrigin(event) {
  const origin = getHeader(event.headers, 'origin')
  if (!origin) {
    return { allowed: false, error: 'No origin provided' };
  }
  if (config.corsAllowedOrigins.indexOf(origin.toLowerCase()) === -1) {
    return { allowed: false, error: 'Not an allowed origin', origin };
  }
  return { allowed: true, origin };
}

function error(error, statusCode=400, errorCode) {
  return {
    statusCode,
    body: JSON.stringify({
      error,
      ...(errorCode ? { error_code: errorCode } : {}),
    })
  };
}

const withCors = fn => async event => {
  const originCheck = getOrigin(event);
  const originHeaders = originCheck.origin
        ? {
          'Access-Control-Allow-Origin': originCheck.origin,
          'Access-Control-Allow-Headers': 'Authorization,Content-Type',
        }
        : {};
  if (!originCheck.allowed) {
    return { ...error(originCheck.error, 400), headers: originHeaders };
  }

  if (event.httpMethod === 'OPTIONS') {
    return {
      statusCode: 200,
      headers: originHeaders,
    };
  }

  const resp = await fn(event);
  return {
    ...resp,
    headers: {
      ...originHeaders,
      ...resp.headers || {},
    },
  }
}

const withSentry = fn => async event => {
  try {
    new URL(process.env.SENTRY_DSN);
    Sentry.init({ dsn: process.env.SENTRY_DSN });
  } catch(err) {
    console.error('Sentry init error', err);
  } finally {
    return fn(event);
  }
}

async function resizeAndUpload(s3, body, prefix, resize=null) {
  console.log('Creating sharp', prefix, resize);
  const s = resize
        ? sharp(body).resize(resize)
        : sharp(body);
  console.log('Created', prefix, resize);
  const buffer = await s.rotate().png().toBuffer();
  console.log('Uploading', prefix, resize);
  return s3.putObject({
    Bucket: process.env.PHOTO_COMP_OUTPUT_BUCKET,
    ContentType: 'image/png',
    ACL: 'public-read',
    Key: `${prefix}x${resize ? resize : 'FULL'}.png`,
    Body: buffer,
  }).promise();
}

const getPhotoUrl = (hash, size) => `https://${process.env.PHOTO_COMP_OUTPUT_DOMAIN}/${hash}x${size}.png`;


module.exports.token = withSentry(withCors(async event => {
  const currentCheck = await checkToken(event);

  if (currentCheck.provided && !currentCheck.signed) {
    return error('token validation error', 401);
  }

  let data;
  try {
    data = JSON.parse(event.body);
  } catch(err) {
    return error('invalid payload');
  }
  if (!data || !data.recaptcha) {
    return error('invalid payload');
  }

  const postData = {
    secret: process.env.PHOTO_COMP_RECAPTCHA_SECRET,
    remoteip: event.requestContext.identity.sourceIp,
    response: data.recaptcha,
  };
  console.log('postData', _.omit(postData, ['secret']));
  const resp = await axios.post(
    'https://www.google.com/recaptcha/api/siteverify',
    Object.entries(postData)
      .map(([k, v]) => `${encodeURIComponent(k)}=${encodeURIComponent(v)}`)
      .join('&'),
  );

  if (!resp.data.success) {
    console.error(resp.data);
    return error('invalid CAPTCHA token', 401);
  }

  let subject;
  if (currentCheck.signed) {
    subject = currentCheck.payload.sub;
  } else {
    subject = `au.org.vicpah.photocomp.${uuid()}`;
  }

  return {
    statusCode: 201,
    body: JSON.stringify({
      token: await createToken(subject),
    }),
  };
}));


module.exports.process = withSentry(async event => {
  console.dir(event, { depth: 20 });
  const s3 = new S3();

  return Promise.all(event.Records.map(async record => {
    const data = await s3.getObject({
      Bucket: record.s3.bucket.name,
      Key: record.s3.object.key,
    }).promise();
    const bodyHash = createHash('blake2s256').update(data.Body).digest('hex');
    await Promise.all([
      resizeAndUpload(s3, data.Body, bodyHash),
      resizeAndUpload(s3, data.Body, bodyHash, 200),
      resizeAndUpload(s3, data.Body, bodyHash, 700),
    ]);
    const photo = await Photo.byHash(bodyHash);
    photo.ready = true;
    await photo.save();
  }));
});
module.exports.upload = withSentry(withCors(async event => {
  const auth = await checkToken(event);
  if (!auth.signed) {
    return error('token validation error', 401);
  }

  let data;
  try {
    data = JSON.parse(event.body);
  } catch(err) {
    return error('must be JSON body');
  }
  if (!data) {
    return error('must be JSON body');
  }
  for (const key of ['name', 'email', 'caption', 'extension', 'hash']) {
    if (!data[key]) {
      return error(`${key}: must be given`);
    }
    if (data[key].length > 200) {
      return error(`${key}: must be <= 200 characters`);
    }
  }
  if (!HEX_RE.test(data.hash)) {
    return error('hash: must be a hex blake2s256 hash of the image');
  }

  console.log('checking for existing', data.hash);
  const existing = await Photo.byHash(data.hash);
  if (existing) {
    console.log('already submitted', data.hash);
    return error('hash: already submitted', 403);
  }

  console.log('inserting new', auth.payload.sub, event.requestContext.identity.sourceIp);
  const photo = new Photo({
      ..._.pick(data, ['name', 'email', 'caption', 'hash']),
      ready: false,
      user_sub: auth.payload.sub,
      user_ip: event.requestContext.identity.sourceIp,
  });

  console.log('getting s3 client');
  const s3 = new S3({
    accessKeyId: process.env.PHOTO_COMP_UPLOAD_KEY,
    secretAccessKey: process.env.PHOTO_COMP_UPLOAD_SECRET,
  });
  const key = `${data.hash}.${data.extension}`;
  console.log('signing data');
  const bodyData = s3.createPresignedPost({
    Bucket: process.env.PHOTO_COMP_UPLOAD_BUCKET,
    Fields: {
      acl: 'private',
      key,
    },
    Conditions: [
      { acl: 'private' },
      { bucket: process.env.PHOTO_COMP_UPLOAD_BUCKET },
      [ 'content-length-range', MIN_UPLOAD_SIZE, MAX_UPLOAD_SIZE ],
    ]
  });

  console.log('saving photo metadata');
  await photo.save();

  console.log('returning response');
  return {
    statusCode: 201,
    body: JSON.stringify(bodyData),
  }
}));
module.exports.getphoto = withSentry(withCors(async event => {
  const photo = await Photo.byHash(event.pathParameters.hash);
  if (!photo) {
    return error('not found', 404);
  }
  return {
    statusCode: 200,
    body: JSON.stringify({
      ..._.pick(photo, ['user_sub', 'name', 'caption', 'hash']),
    }),
  };
}));
module.exports.voteone = withSentry(withCors(async event => {
  const auth = await checkToken(event);
  if (!auth.signed) {
    return error('token validation error', 401);
  }
  const data = {
    hash: event.pathParameters.hash,
    user_sub: auth.payload.sub,
    user_ip: event.requestContext.identity.sourceIp,
  }

  // TODO anti-abuse
  // const userCount = await tx('vote_one')
  //       .count('id')
  //       .where(_.pick(data, ['hash', 'user_sub']))
  //       .first();
  // if (userCount !== 0) {
  //   return { statusCode: 200, body: JSON.stringify({}) };
  // }
  // const ipCount = await tx('vote_one')
  //       .count('id')
  //       .where(_.pick(data, ['hash', 'user_ip']))
  //       .first();
  // if (ipCount > 3) {
  //   return { statusCode: 200, body: JSON.stringify({}) };
  // }

  await new VoteOne(data).save();
  return { statusCode: 200, body: JSON.stringify({}) };
}));
module.exports.voteoneui = withSentry(async event => {
  const { hash } = event.pathParameters;

  const s3 = new S3();

  console.log('loading from s3, and db');
  const indexProm = s3.getObject({
    Bucket: process.env.WEBSITE_BUCKET,
    Key: 'index.html',
  }).promise();

  const photoProm = Photo.byHash(hash);

  const [{ Body: html }, photo] = await Promise.all([indexProm, photoProm]);

  if (!photo) {
    return {
      statusCode: 200,
      body: html,
      headers: { 'Content-Type': 'text/html' },
    }
  }

  console.log('loading html');
  const dom = cheerio.load(html);

  console.log('adding meta');
  const props = [
    ['name', 'og:title', `VicPAH Photo Competition: Photo by ${photo.name}`],
    ['name', 'og:description', photo.caption],
    ['name', 'og:image', getPhotoUrl(hash, 700)],
    ['name', 'og:url', `https://www.vicpah.org.au/photo-comp-2020/vote-for/${hash}/`],
    ['property', 'twitter:card', 'summary_large_image'],
  ]
  dom('head').append(
    props
      .map(([attrName, attrValue, metaValue]) => (
        `<meta ${attrName}="${attrValue}" content="${escape(metaValue)}" />`
      ))
      .join('')
  );

  console.log('dumping html');
  return {
    statusCode: 200,
    body: dom.html(),
    headers: { 'Content-Type': 'text/html' },
  }
});
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max + 1));
}
function randomTrue(prob) {
  return Math.random() < prob;
}
function getNewVoteRace(sortedHashes, doneRaces) {
  const totalRaces = sortedHashes.length * (sortedHashes.length - 1) / 2;
  const goodSetLen = totalRaces - doneRaces.length;

  let lastGood = null;
  let goodIdx = 0;
  let firstIdxMax = sortedHashes.length - 2;
  for (let firstIdxRand = 0; firstIdxRand <= firstIdxMax; firstIdxRand++) {
    let firstIdx = firstIdxRand;
    let secondIdxMax = sortedHashes.length - 1 - firstIdx - 1;
    for (let secondIdxRand = 0; secondIdxRand <= secondIdxMax; secondIdxRand++) {
      let secondIdx = sortedHashes.length - 1 - secondIdxRand;

      const firstHash = sortedHashes[firstIdx];
      const secondHash = sortedHashes[secondIdx];

      if (doneRaces.find(([f,s]) => f === firstHash && s === secondHash)) {
        continue;
      }
      lastGood = [firstHash, secondHash];

      if (randomTrue(1 / (goodSetLen - goodIdx))) {
        return lastGood;
      }

      goodIdx++;
    }
  }

  return lastGood;
}
module.exports.voteraceget = withSentry(withCors(async event => {
  const auth = await checkToken(event);
  if (!auth.signed) {
    return error('token validation error', 401);
  }

  let hashes;
  const lock = await VoteRaceLock.byUserSub(auth.payload.sub);
  if (lock) {
    hashes = lock.hashes;
  } else {
    const [allHashes, userRaces] = await Promise.all([
      Photo.listAllHashes(),
      VoteRace.listHashesByUserSub(auth.payload.sub),
    ]);
    if (allHashes.length < 2) {
      return error('not enough photos yet', 400, 'not_enough');
    }
    const totalRaces = allHashes.length * (allHashes.length - 1) / 2;
    if (userRaces.length >= totalRaces) {
      return error('no more pairs left', 400, 'no_more');
    }

    allHashes.sort();

    const userRacesStr = userRaces.map(raceHashes => raceHashes.join(''));
    while(!hashes || userRacesStr.indexOf(hashes.join('')) !== -1) {
      hashes = getNewVoteRace(allHashes, userRaces).sort();
      console.log('hashes', hashes);
    }

    const newLock = new VoteRaceLock({ user_sub: auth.payload.sub, hashes });
    await newLock.save();
  }
  return {
    statusCode: 201,
    body: JSON.stringify({
      hashes,
    }),
  }
}));
module.exports.voterace = withSentry(withCors(async event => {
  const auth = await checkToken(event);
  if (!auth.signed) {
    return error('token validation error', 401);
  }
  let data;
  try {
    data = JSON.parse(event.body);
  } catch(err) {
    return error('must be JSON body');
  }
  if (!data) {
    return error('must be JSON body');
  }
  for (const key of ['hash']) {
    if (!data[key]) {
      return error(`${key}: must be given`);
    }
    if (data[key].length > 200) {
      return error(`${key}: must be <= 200 characters`);
    }
  }

  const lock = await VoteRaceLock.byUserSub(auth.payload.sub);
  if (!lock) {
    return error('no race in progress');
  }

  if (lock.hashes.indexOf(data.hash) === -1) {
    return error('invalid hash for the current race', 403);
  }

  const race = new VoteRace({
    user_sub: auth.payload.sub,
    user_ip: event.requestContext.identity.sourceIp,
    winner_hash: data.hash,
    other_hash: lock.hashes.indexOf(data.hash) === 0
      ? lock.hashes[1]
      : lock.hashes[0],
  });
  bigLog(await race.save());

  return {
    statusCode: 201,
    body: JSON.stringify({}),
  };
}));
