# VicPAH photo comp

## Deployment

If deploying form MacOS, you can't simply use local `node_modules`, as you need Linux rather than Darwin binaries. This is easy to fix with Docker: `docker run --rm -v $(pwd):/mnt -w /mnt node:12.4 npm install`
