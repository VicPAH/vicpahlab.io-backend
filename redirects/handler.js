'use strict';

const redirects = {
  'vicpah.org.au': 'www.vicpah.org.au',
  'vicpah.com.au': 'www.vicpah.org.au',
  'mz94q4i4e7.execute-api.ap-southeast-2.amazonaws.com': 'www.vicpah.org.au',
};
const statusCode = 302;

module.exports.redirect = async event => {
  const redirectTo = redirects[event.requestContext.domainName];

  if (!redirectTo) {
    return { statusCode: 404, body: 'Unknown domain' }
  }

  return {
    statusCode,
    headers: {
      Location: `https://${redirectTo}${event.path}`,
    },
    body: `Redirecting to ${redirectTo}`,
  };
};
