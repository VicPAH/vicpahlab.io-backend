module.exports = sls => ({
  customDomain: {
    domainName: 'vicpah.org.au',
    basePath: '',
    stage: sls.service.provider.stage,
    createRoute53Record: true,
  },
})
