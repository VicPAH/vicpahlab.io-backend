const jwt = require('jsonwebtoken');
const process = require('process');

const JWK_PEM = Buffer.from(process.env.APPLE_MAPS_KEY_B64, 'base64').toString()
const KEY_ID = '5ZS49YT9Y9'
const TEAM_ID = 'NJ9D5Y2P6P'

const ALLOWED_ORIGINS = [
  'http://localhost:3000',
  'http://192.168.178.32:3000',
  'https://vicpah.gitlab.io',
  'https://vicpah.com.au',
  'https://www.vicpah.com.au',
  'https://vicpah.org.au',
  'https://www.vicpah.org.au',
]

function getHeader(headers, wanted) {
  const wantedLc = wanted.toLowerCase()
  for (const [key, value] of Object.entries(headers)) {
    if (key.toLowerCase() === wantedLc) {
      return value
    }
  }
}

module.exports.jwt = async event => {
  const origin = getHeader(event.headers, 'origin')
  if (!origin) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: 'No origin provided' })
    }
  }
  if (ALLOWED_ORIGINS.indexOf(origin.toLowerCase()) === -1) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: 'Not an allowed origin' })
    }
  }
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': origin,
    },
    body: JSON.stringify({
      token: jwt.sign(
        { origin },
        JWK_PEM,
        {
          algorithm: 'ES256',
          keyid: KEY_ID,
          issuer: TEAM_ID,
          expiresIn: '6h',
        },
      ),
    }),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
