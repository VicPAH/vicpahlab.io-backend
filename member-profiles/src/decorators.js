import knexLib from 'knex';
import _ from 'lodash';
import process from 'process';

import knexfile from '../knexfile';


export const withKnex = handler => async event => {
  let { knex } = event;
  if (_.isNil(knex)) {
    try {
      knex = knexLib(knexfile[process.env.NODE_ENV || 'development']);
    } catch (err) {
      console.error('DB connection error:', err);
      return {
        statusCode: 503,
        body: JSON.stringify({ error: "Couldn't complete the request right now" }),
      };
    }
  }
  return handler({ ...event, knex });
};


export const withBodyData = handler => async event => {
  let bodyData;
  try {
    bodyData = JSON.parse(event.body);
  } catch (err) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: 'Body data must be JSON' }),
    };
  }
  return handler({ ...event, bodyData });
};
