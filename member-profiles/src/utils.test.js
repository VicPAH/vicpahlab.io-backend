import { getCls, getSuper } from './utils';


describe.only('getCls()', () => {
  test('B for new B()', () => {
    class A {}

    class B extends A {}
    expect(getCls(new B())).toBe(B);
  });
});

describe.only('getSuper()', () => {
  test('A for B when B extends A', () => {
    class A {}

    class B extends A {}
    expect(getSuper(B)).toBe(A);
  });
});
