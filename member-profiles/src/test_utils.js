import knexLib from 'knex';
import _ from 'lodash';

import knexfile from '../knexfile';


export const unique = (sep = '-') => `${new Date().valueOf()}${sep}${Math.round(Math.random() * 10000000)}`;


function getNewTestDbConfig() {
  const filename = `/tmp/test-${unique()}.sqlite3`;
  return _.defaultsDeep(
    { connection: { filename } },
    knexfile.test,
  );
}
const getSharedTestDbConfig = _.memoize(getNewTestDbConfig, () => 0);

const createTestDbGetter = getConfig => async () => {
  const knex = knexLib(getConfig());
  await knex.migrate.latest();
  return knex;
};

export const getNewTestDb = createTestDbGetter(getNewTestDbConfig);

export const getSharedTestDb = createTestDbGetter(getSharedTestDbConfig);
