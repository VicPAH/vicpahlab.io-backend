import Joi from '@hapi/joi';
import _ from 'lodash';

// import { callWithTx } from './decorators';
import { ValidationError } from './errors';
import { getCls } from './utils';


export class Model {
  constructor(data) {
    Object.assign(this, data);

    this.getData = this.getData.bind(this);
    this.getDbData = this.getDbData.bind(this);
    this.patchData = this.patchData.bind(this);
    this.validate = this.validate.bind(this);
    this.save = this.save.bind(this);
  }

  getData() {
    return _.pick(this, getCls(this).getFields());
  }

  getDbData() {
    return getCls(this).jsDataToDb(this.getData());
  }

  patchData(data) {
    this.validate({ data, full: false });
    Object.assign(this, data);
  }

  validate({ data, unknown, full = true } = {}) {
    if (!data) {
      data = this;
      if (_.isNil(unknown)) {
        unknown = true;
      }
    }
    if (_.isNil(unknown)) {
      unknown = false;
    }
    const cls = getCls(this);
    const schema = Joi
      .object(full ? cls.getFullDef() : cls.getPartDef())
      .unknown(unknown);
    const { error } = schema.validate(data);
    if (error) {
      throw new ValidationError(error);
    }
  }

  async save({ tx }) {
    this.validate({ full: true });
    const query = tx(getCls(this).getTableName());
    if (!this.id) {
      const [id] = await query
        .insert(this.getDbData())
        .returning('id');
      this.id = id;
    } else {
      await query
        .update(this.getDbData())
        .where({ id: this.id });
    }
  }

  static dbFieldToJs(name) {
    return _.camelCase(name);
  }

  static dbDataToJs(data) {
    return _.mapKeys(
      data,
      (value, key) => this.dbFieldToJs(key),
    );
  }

  static jsFieldToDb(name) {
    return _.snakeCase(name);
  }

  static jsDataToDb(data) {
    return _.mapKeys(
      data,
      (value, key) => this.jsFieldToDb(key),
    );
  }

  static getFields() {
    return _.uniq([
      ...Object.keys(this.getPartDef()),
      ...Object.keys(this.getFullDef()),
    ]);
  }

  static getDbFields() {
    return this.getFields().map(this.jsFieldToDb);
  }

  static getTableName() {
    throw new Error('Must override getTableName');
  }

  static getPartDef() {
    throw new Error('Must override getPartDef');
  }

  static getFullDef() {
    throw new Error('Must override getFullDef');
  }
}
