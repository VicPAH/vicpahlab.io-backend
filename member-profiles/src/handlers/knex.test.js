const { schema } = require('./knex');


describe('schema', () => {
  describe('all opt', () => {
    test('accepted when fn=rollback', () => {
      expect(schema.validate({ fn: 'rollback', all: true }))
        .toEqual({
          value: { fn: 'rollback', all: true },
        });
    });
    test('not accepted when fn=latest', () => {
      expect(schema.validate({ fn: 'latest', all: true }))
        .toEqual(expect.objectContaining({
          error: expect.objectContaining({ message: '"all" is not allowed' }),
        }));
    });
  });
});
