import Joi from '@hapi/joi';
import _ from 'lodash';

import { withKnex } from '../decorators';


export const schema = Joi.object({
  fn: Joi
    .string()
    .only()
    .allow(
      'latest',
      'rollback',
      'up',
      'down',
      'currentVersion',
      'list',
    ),
  all: Joi
    .boolean()
    .when(
      Joi.ref('fn'),
      {
        is: Joi.any().only().allow('rollback'),
        then: Joi.any(),
        otherwise: Joi.any().forbidden(),
      },
    ),
});


export class KnexHandler {
  constructor() {
    this.handle = this.handle.bind(this);
    this.handle = withKnex(this.handle);
  }

  handle(event) {
    const { knex } = event;
    const validated = schema.validate(event, { stripUnknown: true });
    if (validated.error) {
      return validated;
    }

    const { value } = validated;

    const fn = knex.migrate[value.fn].bind(knex.migrate);
    const args = [];
    if (!_.isNil(value.all)) {
      args.push(value.all);
    }
    return fn(...args);
  }
}
