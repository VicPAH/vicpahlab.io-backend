import { withBodyData, withKnex } from '../decorators';
import { createRegisterSchema, detailSchema, Member } from '../member';


export class RegisterHandler {
  constructor() {
    this.handle = withBodyData(this.handle.bind(this));
    this._handleValidated = withKnex(this._handleValidated.bind(this));
  }

  handle(event) {
    const { error, value } = createRegisterSchema.validate(event.bodyData);
    if (error) {
      return { statusCode: 400, body: JSON.stringify(error) };
    }
    return this._handleValidated({ ...event, bodyData: value });
  }

  async _handleValidated({ bodyData, knex, requestContext: { stage } }) {
    return knex.transaction(async tx => {
      if (await Member.byEmail(bodyData.email, { tx })) {
        return {
          statusCode: 400,
          body: JSON.stringify({
            details: [
              {
                message: 'Member exists with that email',
                path: ['email'],
                type: 'object.exists',
              },
            ],
          }),
        };
      }

      const member = new Member({
        ...bodyData,
        joinDate: new Date(),
      });
      await member.save({ tx });
      const { value, error } = detailSchema.validate(member, { stripUnknown: true });

      if (error) {
        console.error('Error dumping:', error);
        return {
          statusCode: 500,
          body: { details: [{ message: 'Unknown error' }] },
        };
      }

      return {
        statusCode: 201,
        body: JSON.stringify(value),
        headers: {
          'Content-Location': `/${stage}/member/${member.id}`,
        },
      };
    });
  }
}
