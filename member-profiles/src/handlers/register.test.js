import _ from 'lodash';

import { Model } from '../model';
import { RegisterHandler } from './register';
import { getSharedTestDb, unique } from '../test_utils';


const getJsMemberData = () => ({
  email: `${unique()}@example.com`,
  displayName: unique(' '),
  legalName: unique(' '),
});
// const getDbMemberData = () => Model.jsDataToDb(getJsMemberData());


describe('RegisterHandler', () => {
  const handler = new RegisterHandler();

  async function runTest(inData, expOutBody) {
    const prom = handler.handle({
      body: JSON.stringify(inData),
    });
    await expect(prom).resolves.toEqual({
      statusCode: 400,
      body: expect.any(String),
    });
    const { body } = await prom;
    expect(JSON.parse(body)).toEqual(expOutBody);
  }
  for (const field of ['email', 'password']) {
    test(`error when "${field}" is not given`, () => runTest(
      _.omit({ ...getJsMemberData(), password: 'testpass' }, [field]),
      expect.objectContaining({
        details: [expect.objectContaining({
          message: `"${field}" is required`,
          path: [field],
          type: 'any.required',
        })],
      }),
    ));
  }
  for (const [field, value] of [
    ['id', 1],
    ['passwordHash', 'tetetet'],
    ['joinDate', new Date()],
    ['committeeNotes', 'tetetet'],
  ]) {
    test(`error when "${field}" is given`, () => runTest(
      {
        ...getJsMemberData(),
        password: 'testpass',
        [field]: value,
      },
      expect.objectContaining({
        details: [expect.objectContaining({
          message: `"${field}" is not allowed`,
          path: [field],
          type: 'object.unknown',
        })],
      }),
    ));
  }
  describe('creates a new member', () => {
    const jsData = getJsMemberData();
    const dbData = Model.jsDataToDb(jsData);
    let knex;
    beforeAll(async () => {
      knex = await getSharedTestDb();
    });

    let prom;
    test('handles without error', async () => {
      prom = handler.handle({
        body: JSON.stringify({ ...jsData, password: 'testpass' }),
        requestContext: { stage: 'test' },
        knex,
      });
      await expect(prom).resolves.toEqual(expect.anything());
    });

    let id;
    test('response contains expected data', async () => {
      await expect(prom).resolves.toEqual(expect.objectContaining({
        statusCode: 201,
        body: expect.any(String),
      }));
      const { body, headers } = await prom;
      const bodyData = JSON.parse(body);
      expect(headers).toEqual({
        'Content-Location': `/test/member/${bodyData.id}`,
      });
      expect(bodyData).toEqual({
        id: expect.any(Number),
        hideMemberList: false,
        joinDate: expect.stringMatching(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}/),
        ...jsData,
      });

      id = bodyData.id;
    });

    test('database contains registered data', async () => {
      await expect(id).toEqual(expect.anything());
      await expect(
        knex('member')
          .select(Object.keys(dbData))
          .where({ id })
          .first(),
      ).resolves.toEqual(dbData);
    });
  });
});
