import _ from 'lodash';

import { Member } from './member';

import { getSharedTestDb, unique } from './test_utils';


async function createMember(knex) {
  const data = {
    email: `${unique()}@example.com`,
    display_name: unique(' '),
    legal_name: 'Test Member',
    hide_member_list: false,
    join_date: new Date(),
  };
  await knex('member').insert(data);
  return data;
}


describe('Member', () => {
  let mockBuild;
  let mockTx;
  beforeEach(() => {
    let isCompleted = false;
    mockTx = jest.fn(() => mockBuild);
    Object.assign(mockTx, {
      commit: jest.fn(() => {
        const orig = isCompleted;
        isCompleted = true;
        return orig ? Promise.reject() : Promise.resolve();
      }),
      rollback: jest.fn(() => {
        const orig = isCompleted;
        isCompleted = true;
        return orig ? Promise.reject() : Promise.resolve();
      }),
      isCompleted: jest.fn(() => isCompleted),
      transacting: jest.fn(() => mockBuild),
    });
    mockBuild = {
      select: jest.fn(() => mockBuild),
      insert: jest.fn(() => mockBuild),
      update: jest.fn(() => mockBuild),
      where: jest.fn(() => mockBuild),
      orWhere: jest.fn(() => mockBuild),
      returning: jest.fn(() => mockBuild),
      first: jest.fn(() => Promise.reject(new Error('no override'))),
    };
  });
  describe('byEmail()', () => {
    test('returns correct data', async () => {
      mockBuild.first.mockImplementation(() => Promise.resolve({
        id: 42,
        email: 'test@example.com',
      }));
      const member = await Member.byEmail('test@example.com', { tx: mockTx });
      expect(member).toEqual(expect.any(Member));
      expect(member).toEqual(expect.objectContaining({ id: 42, email: 'test@example.com' }));
      expect(mockBuild.first).toHaveBeenCalled();
    });
    test('returns null on no result', async () => {
      mockBuild.first.mockImplementation(() => Promise.resolve(null));
      const member = await Member.byEmail('test@example.com', { tx: mockTx });
      expect(member).toBe(null);
    });
  });
  describe('simpleQuery()', () => {
    test('invokation is correct for object where-data', async () => {
      mockBuild.first.mockImplementation(() => Promise.resolve({
        id: 42,
        email: 'test@example.com',
      }));
      await Member.simpleQuery({ email: 'test@example.com' }, { tx: mockTx });
      expect(mockBuild.select).toHaveBeenCalledWith(expect.arrayContaining([
        'id',
        'password_hash',
        'display_name',
        // 'display_pic',
        'legal_name',
        'email',
        'mobile_phone',
        'postal_address',
        'dob',
        'join_date',
        'hide_member_list',
        'committee_notes',
      ]));
      expect(mockBuild.where).toHaveBeenCalledWith({ email: 'test@example.com' });
    });
    test('invokation is correct for array where-data', async () => {
      mockBuild.first.mockImplementation(() => Promise.resolve({
        id: 42,
      }));
      await Member.simpleQuery(
        [
          { email: 'test@example.com' },
          { display_name: 'test' },
          { mobile_phone: '0400000000' },
        ],
        { tx: mockTx },
      );
      expect(mockBuild.where).toHaveBeenCalledWith({ email: 'test@example.com' });
      expect(mockBuild.orWhere).toHaveBeenCalledWith({ display_name: 'test' });
      expect(mockBuild.orWhere).toHaveBeenCalledWith({ mobile_phone: '0400000000' });
    });
    describe('against real DB', () => {
      test('single where, existing item', async () => {
        const knex = await getSharedTestDb();
        const data = await createMember(knex);
        await knex.transaction(async tx => {
          const query = Member.simpleQuery({ email: data.email }, { tx });
          expect(await query.first()).toEqual(expect.objectContaining({
            ..._.pick(data, ['email', 'display_name', 'legal_name']),
          }));
        });
      });
      test('multiple where, existing items', async () => {
        const knex = await getSharedTestDb();
        const datas = [];
        datas.push(await createMember(knex));
        datas.push(await createMember(knex));
        datas.push(await createMember(knex));
        datas.push(await createMember(knex));
        await knex.transaction(async tx => {
          const query = Member.simpleQuery(
            [{ email: datas[1].email }, { email: datas[2].email }],
            { tx },
          );
          expect(await query).toEqual([
            expect.objectContaining({ email: datas[1].email }),
            expect.objectContaining({ email: datas[2].email }),
          ]);
        });
      });
    });
    describe('checkPassword()/setPassword()', () => {
      test("password constructor opt doesn't set password attr", () => {
        const member = new Member({
          email: 'test@example.com',
          password: 'testing',
        });
        expect(member.password).not.toEqual(expect.anything());
      });
      test("setPassword doesn't set password attr", () => {
        const member = new Member({
          email: 'test@example.com',
        });
        member.setPassword('testing');
        expect(member.password).not.toEqual(expect.anything());
      });
      test('check returns true on correct pw through constructor', () => {
        const member = new Member({
          email: 'test@example.com',
        });
        member.setPassword('testing');
        expect(member.checkPassword('testing')).toBe(true);
      });
      test('check returns true on correct pw through set', () => {
        const member = new Member({
          email: 'test@example.com',
          password: 'testing',
        });
        expect(member.checkPassword('testing')).toBe(true);
      });
      test('check returns false on incorrect pw', () => {
        const member = new Member({
          email: 'test@example.com',
          password: 'testing',
        });
        expect(member.checkPassword('not correct')).toBe(false);
      });
      test('hash changes based on email as salt', () => {
        const member1 = new Member({
          email: 'test@example.com',
          password: 'testing',
        });
        const member2 = new Member({
          email: 'test@example.com',
          password: 'testing',
        });
        const member3 = new Member({
          email: 'other@example.com',
          password: 'testing',
        });

        // Check the full string
        expect(member1.passwordHash).toEqual(member2.passwordHash);
        expect(member1.passwordHash).not.toEqual(member3.passwordHash);

        // Check the actual hash
        const member1Obj = Member._parsePasswordHashStr(member1.passwordHash);
        const member2Obj = Member._parsePasswordHashStr(member2.passwordHash);
        const member3Obj = Member._parsePasswordHashStr(member3.passwordHash);
        expect(member1Obj.passwordHash).toEqual(member2Obj.passwordHash);
        expect(member1Obj.passwordHash).not.toEqual(member3Obj.passwordHash);
      });
    });
  });
});
