export const getCls = obj => Object.getPrototypeOf(obj).constructor;

export const getSuper = cls => Object.getPrototypeOf(cls);
