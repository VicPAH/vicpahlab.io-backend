import Joi from '@hapi/joi';
import _ from 'lodash';

import { ValidationError } from './errors';
import { Model } from './model';


describe('Model', () => {
  describe('field converters', () => {
    const dbName = 'test_a_field';
    const jsName = 'testAField';
    test('dbFieldToJs', () => {
      expect(Model.dbFieldToJs(dbName)).toEqual(jsName);
    });
    test('jsFieldToDb', () => {
      expect(Model.jsFieldToDb(jsName)).toEqual(dbName);
    });
  });

  describe('data converters', () => {
    const dbData = { test_field_1: 'the value', test_field_2: 'the 2nd value' };
    const jsData = { testField1: 'the value', testField2: 'the 2nd value' };
    test('dbDataToJs', () => {
      expect(Model.dbDataToJs(dbData)).toEqual(jsData);
    });
    test('jsDataToDb', () => {
      expect(Model.jsDataToDb(jsData)).toEqual(dbData);
    });
  });

  describe('subclass-dependant', () => {
    class Test extends Model {
      static getPartDef() {
        return {
          testField1: Joi.string(),
          testField2: Joi.string(),
        };
      }

      static getFullDef() {
        return {
          testField1: Joi.string().required(),
          testField3: Joi.string().required(),
        };
      }
    }
    const fieldsTestData = {
      testField1: 'test value 1',
      testField2: 'test value 2',
      testField3: 'test value 3',
    };
    const extraTestData = {
      ...fieldsTestData,
      testField4: 'test value 4',
    };
    test('getFields()', () => {
      expect(Test.getFields()).toEqual(['testField1', 'testField2', 'testField3']);
    });
    test('getDbFields()', () => {
      expect(Test.getDbFields()).toEqual(['test_field_1', 'test_field_2', 'test_field_3']);
    });

    test('getData()', () => {
      const obj = new Test(extraTestData);
      expect(obj.getData()).toEqual(fieldsTestData);
    });
    test('getDbData()', () => {
      const obj = new Test(extraTestData);
      expect(obj.getDbData()).toEqual({
        test_field_1: fieldsTestData.testField1,
        test_field_2: fieldsTestData.testField2,
        test_field_3: fieldsTestData.testField3,
      });
    });

    describe('validate()', () => {
      test('partial, success', () => {
        const obj = new Test({});
        expect(() => obj.validate({ full: false })).not.toThrow();
      });
      test('partial, fail', () => {
        const obj = new Test({});
        const data = { testField1: 222 };
        expect(() => obj.validate({ full: false, data })).toThrow(ValidationError);
        expect(() => obj.validate({ full: false, data })).toThrow('"testField1" must be a string');
      });
      test('full, success', () => {
        const obj = new Test({});
        expect(() => obj.validate({
          full: true,
          data: _.pick(extraTestData, ['testField1', 'testField3']),
        })).not.toThrow();
      });
      test('full, fail', () => {
        const obj = new Test({});
        expect(() => obj.validate()).toThrow(ValidationError);
        expect(() => obj.validate({ fulL: true })).toThrow('"testField1" is required');
      });
      test('data from opt', () => {
        const obj = new Test({});
        expect(() => obj.validate({ full: false })).not.toThrow();
        expect(() => obj.validate({ full: false, data: { testField1: 222 } })).toThrow(ValidationError);
      });
      test('data from props', () => {
        const obj = new Test({});
        expect(() => obj.validate({ full: false })).not.toThrow();
        obj.testField1 = 222;
        expect(() => obj.validate({ full: false })).toThrow(ValidationError);
      });
    });

    describe('patchData()', () => {
      test('validates input data', () => {
        const obj = new Test({});
        expect(() => obj.patchData({ testField1: 222 })).toThrow(ValidationError);
        expect(obj.testField1).not.toEqual(222);
      });
      test('allows partial data', () => {
        const obj = new Test({});
        expect(() => obj.patchData({ testField1: 'good data' })).not.toThrow();
        expect(obj.testField1).toEqual('good data');
      });
    });
    describe('save()', () => {
      test('validates full data', async () => {
        const obj = new Test({});
        await expect(obj.save({ tx: { isCompleted: () => true } })).rejects.toThrow(ValidationError);
      });
    });
  });
});
