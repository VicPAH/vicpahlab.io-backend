import crypto from 'crypto';
import Joi from '@hapi/joi';
import _ from 'lodash';

import { Model } from './model';
import { getCls } from './utils';


const PASSWORD_HASH_NAME = 'sha256';
const PASSWORD_HASH_OPTS = {};


const sharedItems = {
  displayName: Joi.string().min(3).max(31),
  legalName: Joi.string().min(3).max(255),
  email: Joi.string().email(),
  mobilePhone: Joi.string().max(255),
  postalAddress: Joi.string().max(255),
  dob: Joi.date().greater('1920-01-01').less('now').iso(),
  hideMemberList: Joi.boolean().default(false),
};
const readItems = {
  id: Joi.number().integer(),
  joinDate: Joi.date().iso(),
};
const writeItems = {
  password: Joi.string().min(5).max(255),
};
const adminItems = {
  committeeNotes: Joi.string(),
};


const dbPartItems = {
  ...sharedItems,
  ...readItems,
  ...adminItems,
  passwordHash: Joi.string(),
};
const dbFullItems = { // TODO add required fields
  ...dbPartItems,
  ..._.chain(dbPartItems)
    .pick(['displayName', 'legalName', 'joinDate', 'hideMemberList'])
    .mapValues((field, name) => field.required())
    .value(),
};


export const detailItems = {
  ...sharedItems,
  ...readItems,
};

export const detailAdminItems = {
  ...sharedItems,
  ...readItems,
  ...adminItems,
};

export const listItems = _.pick(detailItems, ['id', 'displayName']);


const createItemsBase = {
  ...sharedItems,
  ...writeItems,
};
const createItems = {
  ...createItemsBase,
  ..._.chain(createItemsBase)
    .pick(['displayName', 'legalName'])
    .mapValues((field, name) => field.required())
    .value(),
};

const createRegisterItems = {
  ...createItems,
  ..._.chain(createItems)
    .pick(['password', 'email'])
    .mapValues((field, name) => field.required())
    .value(),
};

export const createAdminItems = {
  ...createItems,
  ...adminItems,
};


export const updateItems = {
  ...sharedItems,
  ...writeItems,
};

export const updateAdminItems = {
  ...updateItems,
  ...adminItems,
};


// 'id',
// 'password_hash',  // n
// 'display_name',
// 'display_pic',  // n
// 'legal_name',
// 'email',  // n
// 'mobile_phone',  // n
// 'postal_address',  // n
// 'dob',  // n
// 'join_date',
// 'hide_member_list',
// 'committee_notes',  // n
// export const dbFullSchema = Joi.object(dbFullItems);

// export const dbPartSchema = Joi.object(dbPartItems);

export const detailSchema = Joi.object(detailItems);

export const detailAdminSchema = Joi.object(detailAdminItems);

export const listSchema = Joi.array().items(listItems);

export const createRegisterSchema = Joi.object(createRegisterItems);

export const createAdminSchema = Joi.object(createAdminItems);

export const updateSchema = Joi.object(updateItems);

export const updateAdminSchema = Joi.object(updateAdminItems);


export class Member extends Model {
  constructor(data) {
    super(_.omit(data, ['password']));

    this.setPassword = this.setPassword.bind(this);
    this.checkPassword = this.checkPassword.bind(this);
    // this.checkAndUpdatePassword = this.checkAndUpdatePassword.bind(this);

    if (data.password) {
      this.setPassword(data.password);
    }
  }

  static getTableName() {
    return 'member';
  }

  static getPartDef() {
    return dbPartItems;
  }

  static getFullDef() {
    return dbFullItems;
  }

  static _passwordHashObj(password, hashName, opts, salt) {
    const hash = crypto.createHash(hashName);
    hash.update(password);
    hash.update(Buffer.from([0]));
    hash.update(salt);
    return { hashName, opts, salt, passwordHash: hash.digest('base64') };
  }

  static _passwordHashStr(...args) {
    const hashObj = this._passwordHashObj.call(this, ...args);
    return [
      hashObj.hashName,
      JSON.stringify(hashObj.opts),
      hashObj.salt,
      hashObj.passwordHash,
    ].join(';');
  }

  static _parsePasswordHashStr(str) {
    const [hashName, optsStr, salt, passwordHash] = str.split(';');
    return {
      hashName,
      passwordHash,
      salt,
      opts: JSON.parse(optsStr),
    };
  }

  setPassword(password) {
    const cls = getCls(this);
    this.passwordHash = cls._passwordHashStr.call(
      cls, password, PASSWORD_HASH_NAME, PASSWORD_HASH_OPTS, this.email,
    );
  }

  checkPassword(password) {
    const cls = getCls(this);
    const dbHashObj = cls._parsePasswordHashStr.call(cls, this.passwordHash);
    const thisHashObj = cls._passwordHashObj.call(
      cls, password, dbHashObj.hashName, dbHashObj.opts, dbHashObj.salt,
    );
    return dbHashObj.passwordHash === thisHashObj.passwordHash;
  }
  // async checkAndUpdatePassword(password, { tx }) {
  //   if (this.checkPassword(password)) {
  //     const currentHash = this.passwordHash;
  //     this.setPassword(password);

  //     if (this.id && tx) {
  //       if (currentHash !== this.passwordHash) {
  //         await tx(this.getTableName())
  //           .update({ passwordHash: this.passwordHash })
  //           .where({ id: this.id });
  //       }
  //     }
  //     return true;
  //   }
  //   return false;
  // }

  static simpleQuery(data, { tx }) {
    let query = tx(this.getTableName())
      .select(this.getDbFields());
    if (!_.isArray(data)) {
      data = [data];
    }

    const [firstWhere, ...orWheres] = data;
    query = query.where(firstWhere);
    for (const orWhere of orWheres) {
      query = query.orWhere(orWhere);
    }

    return query;
  }

  static async simpleQueryFirst(data, opts) {
    const query = this.simpleQuery(data, opts);
    const row = await query.first();
    if (!row) {
      return null;
    }
    return new this(this.dbDataToJs(row), opts.knex);
  }

  static async byEmail(val, opts) {
    return this.simpleQueryFirst({ email: val }, opts);
  }

  static async byId(val, opts) {
    return this.simpleQueryFirst({ id: val }, opts);
  }
}
// TODO don't bind to Member, because this breaks subclasses
Member.simpleQuery = Member.simpleQuery.bind(Member);
Member.simpleQueryFirst = Member.simpleQueryFirst.bind(Member);
Member.byEmail = Member.byEmail.bind(Member);
Member.byId = Member.byId.bind(Member);
