#!/usr/bin/env node

const Eckles = require('eckles'); // eslint-disable-line import/no-extraneous-dependencies

/* eslint-disable no-console */
Eckles
  .generate({ format: 'jwk' })
  .then(keypair => {
    console.log(
      'MEMBER_PROFILES_JWK_B64:',
      Buffer.from(JSON.stringify(keypair)).toString('base64'),
    );
    console.log(
      'WEBSITE_JWKS_B64:',
      Buffer.from(JSON.stringify({
        keys: [{
          use: 'sig',
          ...keypair.public,
        }],
      })).toString('base64'),
    );
  });
