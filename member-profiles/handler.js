import { KnexHandler } from './src/handlers/knex';
import { RegisterHandler } from './src/handlers/register';


class Handler {
  static async register(event) {
    return new RegisterHandler().handle(event);
  }

  static async knex(event) {
    return new KnexHandler().handle(event);
  }
}


module.exports = Handler;
