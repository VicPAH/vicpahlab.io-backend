module.exports.up = knex => (
  knex.schema
    .createTable('member', table => {
      table.increments('id');
      table.string('email', 255).nullable();
      table.string('password_hash', 255).nullable();

      table.string('display_name', 255).notNullable();
      table.string('display_pic', 255).nullable();
      table.string('legal_name', 255).notNullable();

      table.string('mobile_phone', 16).nullable();
      table.text('postal_address').nullable();
      table.date('dob').nullable();
      table.date('join_date').notNullable();

      table.boolean('hide_member_list').notNullable();

      table.text('committee_notes').nullable();
    })
    .createTable('role', table => {
      table.integer('member_id').unsigned().notNullable();
      table.string('role', 16).notNullable();
      table.foreign('member_id').references('id').inTable('member');
      table.primary(['member_id']);
    })
);

module.exports.down = knex => (
  knex.schema
    .dropTable('role')
    .dropTable('member')
);
