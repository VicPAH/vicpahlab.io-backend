import mysqlDialect from 'knex/lib/dialects/mysql/index';
import sqliteDialect from 'knex/lib/dialects/sqlite3/index';
import process from 'process';


const sqliteConfig = {
  client: sqliteDialect,
  connection: {
    filename: process.env.NODE_ENV === 'test'
      ? `/tmp/test-${new Date().valueOf()}.sqlite3`
      : `./${process.env.NODE_ENV || 'development'}.sqlite3`,
  },
};
const mysqlConfig = {
  client: mysqlDialect,
  connection: {
    host: process.env.MEMBER_PROFILES_DB_ADDR,
    port: process.env.MEMBER_PROFILES_DB_PORT,
    user: process.env.MEMBER_PROFILES_DB_USER,
    password: process.env.MEMBER_PROFILES_DB_PASS,
    database: process.env.MEMBER_PROFILES_DB_NAME,
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: `${__dirname}/migrations`,
  },
};


// We have a local dev, and a lambda dev
if (process.env.MEMBER_PROFILES_DB_ADDR) {
  module.exports = {
    development: mysqlConfig,
    production: mysqlConfig,
    test: sqliteConfig,
  };
} else {
  module.exports = {
    development: sqliteConfig,
    test: sqliteConfig,
  };
}
