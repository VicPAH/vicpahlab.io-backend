module.exports = sls => ({
  nodeEnv: {
    dev: 'development',
    prod: 'production',
  }[sls.service.provider.stage],
});
